# FDMModeAnalysis

## About

`FDMModeAnalysis.jl` is a set of utilities to assist in analyzing the
results from the
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
mode solver package.

## Installation

This package is not in the official Julia package repository, so to
install, run in Julia:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDMModeAnalysis.jl
```

## Documentation

Package documentation and usage examples can be found at the
[Gitlab Pages for FDMModeAnalysis.jl](https://pawelstrzebonski.gitlab.io/FDMModeAnalysis.jl/).
