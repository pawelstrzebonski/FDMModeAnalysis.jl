"""
    dot(a, b)

Re-implement dot to avoid bring it in.
dot(a, b)=sum(conj.(a).*b)
"""
dot(a, b) = sum(conj.(a) .* b)

"""
    overlapfactor(dx[, dy], mode, active)

Calculate the fraction of modal power of modal field(s) `mode` that
overlaps with the bitmask `active`, given grid sampling `dx` (and `dy`
for 2D problems).
"""
function overlapfactor(dx::AbstractVector, mode::AbstractVector, active::AbstractVector)
    @assert size(dx) == size(mode) == size(active)
    dot(dx, abs2.(mode) .* active) / dot(dx, abs2.(mode))
end
function overlapfactor(dx::Number, mode::AbstractVector, active::AbstractVector)
    @assert size(mode) == size(active)
    sum(@. dx * abs2(mode) * active) / sum(@. dx * abs2(mode))
end
function overlapfactor(
    dx::AbstractVector,
    dy::AbstractVector,
    mode::AbstractMatrix,
    active::AbstractMatrix,
)
    @assert size(mode) == size(active) == (length(dx), length(dy))
    dot(dx * dy', abs2.(mode) .* active) / dot(dx * dy', abs2.(mode))
end
function overlapfactor(dx::Number, dy::Number, mode::AbstractMatrix, active::AbstractMatrix)
    @assert size(mode) == size(active)
    sum(@. dx * dy * abs2(mode) * active) / sum(@. dx * dy * abs2(mode))
end
function overlapfactor(dx, modes::AbstractMatrix, active::AbstractVector)
    [overlapfactor(dx, modes[:, i], active) for i = 1:size(modes, 2)]
end
function overlapfactor(dx, dy, modes::AbstractArray, active::AbstractMatrix)
    @assert ndims(modes) == 3
    [overlapfactor(dx, dy, modes[:, :, i], active) for i = 1:size(modes, 3)]
end
