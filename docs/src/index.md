# FDMModeAnalysis.jl

## About

`FDMModeAnalysis` provides various utilities for analyzing the results
of the modes calculated by the
[`FDMModes`](https://gitlab.com/pawelstrzebonski/FDMModes.jl)
mode solver.

## Installation

This package is not in the official Julia package repository, so to
install this package run the following command in the Julia REPL:

```Julia
]add https://gitlab.com/pawelstrzebonski/FDMModeAnalysis.jl
```

## Features

* Mode overlap integral/factor calculation (such as for mode confinement or gain-overlap factor calculations)
