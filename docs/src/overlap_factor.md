# Overlap Factor Calculation

Functions for calculating the mode power fraction that overlaps with a
specified region.

```@autodocs
Modules = [FDMModeAnalysis]
Pages   = ["overlap_factor.jl"]
```
