using Documenter
import FDMModeAnalysis

makedocs(
    sitename = "FDMModeAnalysis.jl",
    repo = Documenter.Remotes.GitLab("pawelstrzebonski", "FDMModeAnalysis.jl"),
    pages = [
        "Home" => "index.md",
        "Examples and Usage" => ["Basic Usage" => "example.md"],
        "References" => "references.md",
        "Contributing" => "contributing.md",
        "src/" => ["overlap_factor.jl" => "overlap_factor.md"],
        "test/" => ["overlap_factor.jl" => "overlap_factor_test.md"],
    ],
)
